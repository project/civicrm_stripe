DESCRIPTION
------------
Stripe Payment Processor integration for CiviCRM

INSTRUCTIONS
------------
There are no special steps required in Drupal.
Turn this module on and follow the steps required on CiviCRM's side.

Installing Stripe as a payment processor in CiviCRM 4.x:

Copy Stripe's PHP library folder 'stripe-php' to civicrm/packages/stripe-php 
You can get Stripe's PHP library here: https://github.com/stripe/stripe-php

The CiviCRM Stripe extension can be downloaded here: 
https://github.com/drastik/civicrm_stripe
